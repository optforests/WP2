CHOROLOGICAL MAP OF ACER HELDREICHII

Shapefiles:
Acer_heldreichii_heldrechii_plg.shp: Acer heldreichii heldrechii native range (polygon)
Acer_heldreichii_heldrechii_pnt.shp: Acer heldreichii heldrechii isolated populations (point)
Acer_heldreichii_trautvetteri_plg.shp: Acer heldreichii trautvetteri native range (polygon)
Acer_heldreichii_trautvetteri_pnt.shp: Acer heldreichii trautvetteri isolated populations (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Acer_heldreichii_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 15 Feb 2022