CHOROLOGICAL MAP OF PRUNUS MAHALEB

Shapefiles:
Prunus_mahaleb_plg.shp: Prunus mahaleb native range (polygon)
Prunus_mahaleb_plg_clip.shp: Prunus mahaleb native range clipped with coastline (polygon)
Prunus_mahaleb_pnt.shp: Prunus mahaleb isolated populations (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Prunus_mahaleb_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 30 May 2023