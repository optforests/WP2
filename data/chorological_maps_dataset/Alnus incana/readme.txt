CHOROLOGICAL MAP OF ALNUS INCANA

Shapefiles:
Alnus_incana_hirsuta_plg.shp: Alnus incana hirsuta native range (polygon)
Alnus_incana_hirsuta_plg_clip.shp: Alnus incana hirsuta native range clipped with coastline (polygon)
Alnus_incana_hirsuta_pnt.shp: Alnus incana hirsuta isolated populations (point)
Alnus_incana_incana_plg.shp: Alnus incana incana native range (polygon)
Alnus_incana_incana_plg_clip.shp: Alnus incana incana native range clipped with coastline (polygon)
Alnus_incana_incana_pnt.shp: Alnus incana incana isolated populations (point)
Alnus_incana_rugosa_plg.shp: Alnus incana rugosa native range (polygon)
Alnus_incana_rugosa_plg_clip.shp: Alnus incana rugosa native range clipped with coastline (polygon)
Alnus_incana_rugosa_pnt.shp: Alnus incana rugosa isolated populations (point)
Alnus_incana_tenuifolia_plg.shp: Alnus incana tenuifolia native range (polygon)
Alnus_incana_tenuifolia_pnt.shp: Alnus incana tenuifolia isolated populations (point)
Alnus_incana_incana_syn_pnt.shp: Alnus incana incana introduced and naturalized (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Alnus_incana_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 13 Dec 2018