CHOROLOGICAL MAP OF CUPRESSUS SEMPERVIRENS

Shapefiles:
Cupressus_sempervirens_med_plg.shp: Cupressus sempervirens med native range (polygon)
Cupressus_sempervirens_med_plg_clip.shp: Cupressus sempervirens med native range clipped with coastline (polygon)
Cupressus_sempervirens_plg.shp: Cupressus sempervirens native range (polygon)
Cupressus_sempervirens_plg_clip.shp: Cupressus sempervirens native range clipped with coastline (polygon)
Cupressus_sempervirens_pnt.shp: Cupressus sempervirens isolated populations (point)
Cupressus_sempervirens_syn_plg.shp: Cupressus sempervirens introduced and naturalized (polygon)
Cupressus_sempervirens_syn_plg_clip.shp: Cupressus sempervirens introduced and naturalized clipped with coastline (polygon)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Cupressus_sempervirens_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 13 Dec 2018