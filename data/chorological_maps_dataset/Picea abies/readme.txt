CHOROLOGICAL MAP OF PICEA ABIES

Shapefiles:
Picea_abies_plg.shp: Picea abies native range (polygon)
Picea_abies_plg_clip.shp: Picea abies native range clipped with coastline (polygon)
Picea_abies_pnt.shp: Picea abies isolated populations (point)
Picea_abies_syn_plg.shp: Picea abies introduced and naturalized (polygon)
Picea_abies_syn_plg_clip.shp: Picea abies introduced and naturalized clipped with coastline (polygon)
Picea_abies_syn_pnt.shp: Picea abies introduced and naturalized (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Picea_abies_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 13 Dec 2018