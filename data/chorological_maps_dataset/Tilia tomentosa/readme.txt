CHOROLOGICAL MAP OF TILIA TOMENTOSA

Shapefiles:
Tilia_tomentosa_plg.shp: Tilia tomentosa native range (polygon)
Tilia_tomentosa_plg_clip.shp: Tilia tomentosa native range clipped with coastline (polygon)
Tilia_tomentosa_pnt.shp: Tilia tomentosa isolated populations (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Tilia_tomentosa_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 29 Mar 2023