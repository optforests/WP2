CHOROLOGICAL MAP OF SORBUS ARIA

Shapefiles:
Sorbus_aria_complex_plg.shp: Sorbus aria complex native range (polygon)
Sorbus_aria_complex_plg_clip.shp: Sorbus aria complex native range clipped with coastline (polygon)
Sorbus_aria_plg.shp: Sorbus aria native range (polygon)
Sorbus_aria_plg_clip.shp: Sorbus aria native range clipped with coastline (polygon)
Sorbus_aria_pnt.shp: Sorbus aria isolated populations (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Sorbus_aria_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 13 Dec 2018