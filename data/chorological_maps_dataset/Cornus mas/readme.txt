CHOROLOGICAL MAP OF CORNUS MAS

Shapefiles:
Cornus_mas_plg.shp: Cornus mas native range (polygon)
Cornus_mas_plg_clip.shp: Cornus mas native range clipped with coastline (polygon)
Cornus_mas_pnt.shp: Cornus mas isolated populations (point)
Cornus_mas_syn_pnt.shp: Cornus mas introduced and naturalized (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Cornus_mas_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 13 Dec 2018