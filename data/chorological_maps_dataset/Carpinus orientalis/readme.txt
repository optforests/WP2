CHOROLOGICAL MAP OF CARPINUS ORIENTALIS

Shapefiles:
Carpinus_orientalis_plg.shp: Carpinus orientalis native range (polygon)
Carpinus_orientalis_plg_clip.shp: Carpinus orientalis native range clipped with coastline (polygon)
Carpinus_orientalis_pnt.shp: Carpinus orientalis isolated populations (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Carpinus_orientalis_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 13 Dec 2018