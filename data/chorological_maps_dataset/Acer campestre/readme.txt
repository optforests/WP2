CHOROLOGICAL MAP OF ACER CAMPESTRE

Shapefiles:
Acer_campestre_plg.shp: Acer campestre native range (polygon)
Acer_campestre_plg_clip.shp: Acer campestre native range clipped with coastline (polygon)
Acer_campestre_pnt.shp: Acer campestre isolated populations (point)
Acer_campestre_syn_pnt.shp: Acer campestre introduced and naturalized (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Acer_campestre_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 13 Dec 2018