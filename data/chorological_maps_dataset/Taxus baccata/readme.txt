CHOROLOGICAL MAP OF TAXUS BACCATA

Shapefiles:
Taxus_baccata_plg.shp: Taxus baccata native range (polygon)
Taxus_baccata_plg_clip.shp: Taxus baccata native range clipped with coastline (polygon)
Taxus_baccata_pnt.shp: Taxus baccata isolated populations (point)
Taxus_baccata_syn_plg.shp: Taxus baccata introduced and naturalized (polygon)
Taxus_baccata_syn_plg_clip.shp: Taxus baccata introduced and naturalized clipped with coastline (polygon)
Taxus_baccata_syn_pnt.shp: Taxus baccata introduced and naturalized (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Taxus_baccata_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 06 Jun 2022