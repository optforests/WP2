CHOROLOGICAL MAP OF PINUS PINEA

Shapefiles:
Pinus_pinea_plg.shp: Pinus pinea native range (polygon)
Pinus_pinea_plg_clip.shp: Pinus pinea native range clipped with coastline (polygon)
Pinus_pinea_pnt.shp: Pinus pinea isolated populations (point)
Pinus_pinea_syn_plg.shp: Pinus pinea introduced and naturalized (polygon)
Pinus_pinea_syn_plg_clip.shp: Pinus pinea introduced and naturalized clipped with coastline (polygon)
Pinus_pinea_syn_pnt.shp: Pinus pinea introduced and naturalized (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Pinus_pinea_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 27 May 2020