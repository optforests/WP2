CHOROLOGICAL MAP OF OLEA EUROPAEA

Shapefiles:
Olea_europaea_cerasiformis_pnt.shp: Olea europaea cerasiformis isolated populations (point)
Olea_europaea_europaea_plg.shp: Olea europaea europaea native range (polygon)
Olea_europaea_europaea_plg_clip.shp: Olea europaea europaea native range clipped with coastline (polygon)
Olea_europaea_europaea_pnt.shp: Olea europaea europaea isolated populations (point)
Olea_europaea_guanchica_pnt.shp: Olea europaea guanchica isolated populations (point)
Olea_europaea_laperrinei_plg.shp: Olea europaea laperrinei native range (polygon)
Olea_europaea_laperrinei_pnt.shp: Olea europaea laperrinei isolated populations (point)
Olea_europaea_maroccana_pnt.shp: Olea europaea maroccana isolated populations (point)
Olea_europaea_europaea_syn_pnt.shp: Olea europaea europaea introduced and naturalized (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Olea_europaea_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 13 Dec 2018