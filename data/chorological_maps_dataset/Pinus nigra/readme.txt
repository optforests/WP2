CHOROLOGICAL MAP OF PINUS NIGRA

Shapefiles:
Pinus_nigra_dalmatica_pnt.shp: Pinus nigra dalmatica isolated populations (point)
Pinus_nigra_laricio_plg.shp: Pinus nigra laricio native range (polygon)
Pinus_nigra_laricio_pnt.shp: Pinus nigra laricio isolated populations (point)
Pinus_nigra_nigra_plg.shp: Pinus nigra nigra native range (polygon)
Pinus_nigra_nigra_plg_clip.shp: Pinus nigra nigra native range clipped with coastline (polygon)
Pinus_nigra_nigra_pnt.shp: Pinus nigra nigra isolated populations (point)
Pinus_nigra_pallasiana_plg.shp: Pinus nigra pallasiana native range (polygon)
Pinus_nigra_pallasiana_plg_clip.shp: Pinus nigra pallasiana native range clipped with coastline (polygon)
Pinus_nigra_pallasiana_pnt.shp: Pinus nigra pallasiana isolated populations (point)
Pinus_nigra_salzmannii_plg.shp: Pinus nigra salzmannii native range (polygon)
Pinus_nigra_salzmannii_pnt.shp: Pinus nigra salzmannii isolated populations (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Pinus_nigra_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 11 Jun 2021