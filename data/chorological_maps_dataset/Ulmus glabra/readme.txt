CHOROLOGICAL MAP OF ULMUS GLABRA

Shapefiles:
Ulmus_glabra_plg.shp: Ulmus glabra native range (polygon)
Ulmus_glabra_plg_clip.shp: Ulmus glabra native range clipped with coastline (polygon)
Ulmus_glabra_pnt.shp: Ulmus glabra isolated populations (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Ulmus_glabra_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 14 Nov 2022