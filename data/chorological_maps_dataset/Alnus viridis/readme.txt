CHOROLOGICAL MAP OF ALNUS VIRIDIS

Shapefiles:
Alnus_viridis_crispa_plg.shp: Alnus viridis crispa native range (polygon)
Alnus_viridis_crispa_plg_clip.shp: Alnus viridis crispa native range clipped with coastline (polygon)
Alnus_viridis_crispa_pnt.shp: Alnus viridis crispa isolated populations (point)
Alnus_viridis_fruticosa_plg.shp: Alnus viridis fruticosa native range (polygon)
Alnus_viridis_fruticosa_plg_clip.shp: Alnus viridis fruticosa native range clipped with coastline (polygon)
Alnus_viridis_fruticosa_pnt.shp: Alnus viridis fruticosa isolated populations (point)
Alnus_viridis_sinuata_plg.shp: Alnus viridis sinuata native range (polygon)
Alnus_viridis_sinuata_plg_clip.shp: Alnus viridis sinuata native range clipped with coastline (polygon)
Alnus_viridis_suaveolens_pnt.shp: Alnus viridis suaveolens isolated populations (point)
Alnus_viridis_viridis_plg.shp: Alnus viridis viridis native range (polygon)
Alnus_viridis_viridis_pnt.shp: Alnus viridis viridis isolated populations (point)
Alnus_viridis_viridis_syn_pnt.shp: Alnus viridis viridis introduced and naturalized (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Alnus_viridis_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 13 Dec 2018