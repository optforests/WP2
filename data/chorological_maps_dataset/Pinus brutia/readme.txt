CHOROLOGICAL MAP OF PINUS BRUTIA

Shapefiles:
Pinus_brutia_brutia_plg.shp: Pinus brutia brutia native range (polygon)
Pinus_brutia_brutia_plg_clip.shp: Pinus brutia brutia native range clipped with coastline (polygon)
Pinus_brutia_brutia_pnt.shp: Pinus brutia brutia isolated populations (point)
Pinus_brutia_eldarica_plg.shp: Pinus brutia eldarica native range (polygon)
Pinus_brutia_eldarica_pnt.shp: Pinus brutia eldarica isolated populations (point)
Pinus_brutia_pendulifolia_pnt.shp: Pinus brutia pendulifolia isolated populations (point)
Pinus_brutia_pityusa_plg.shp: Pinus brutia pityusa native range (polygon)
Pinus_brutia_pityusa_plg_clip.shp: Pinus brutia pityusa native range clipped with coastline (polygon)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Pinus_brutia_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 11 Jun 2021