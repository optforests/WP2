CHOROLOGICAL MAP OF JUNIPERUS EXCELSA

Shapefiles:
Juniperus_excelsa_excelsa_plg.shp: Juniperus excelsa excelsa native range (polygon)
Juniperus_excelsa_excelsa_plg_clip.shp: Juniperus excelsa excelsa native range clipped with coastline (polygon)
Juniperus_excelsa_excelsa_pnt.shp: Juniperus excelsa excelsa isolated populations (point)
Juniperus_excelsa_polycarpos_plg.shp: Juniperus excelsa polycarpos native range (polygon)
Juniperus_excelsa_polycarpos_pnt.shp: Juniperus excelsa polycarpos isolated populations (point)
Juniperus_excelsa_seravschanica_plg.shp: Juniperus excelsa seravschanica native range (polygon)
Juniperus_excelsa_seravschanica_pnt.shp: Juniperus excelsa seravschanica isolated populations (point)
Juniperus_excelsa_turcomanica_plg.shp: Juniperus excelsa turcomanica native range (polygon)
Juniperus_excelsa_turcomanica_pnt.shp: Juniperus excelsa turcomanica isolated populations (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Juniperus_excelsa_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 11 Jan 2021