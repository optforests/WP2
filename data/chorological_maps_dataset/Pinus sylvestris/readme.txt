CHOROLOGICAL MAP OF PINUS SYLVESTRIS

Shapefiles:
Pinus_sylvestris_plg.shp: Pinus sylvestris native range (polygon)
Pinus_sylvestris_plg_clip.shp: Pinus sylvestris native range clipped with coastline (polygon)
Pinus_sylvestris_pnt.shp: Pinus sylvestris isolated populations (point)
Pinus_sylvestris_syn_plg.shp: Pinus sylvestris introduced and naturalized (polygon)
Pinus_sylvestris_syn_plg_clip.shp: Pinus sylvestris introduced and naturalized clipped with coastline (polygon)
Pinus_sylvestris_syn_pnt.shp: Pinus sylvestris introduced and naturalized (point)

Coastline: derived from Natural Earth dataset Admin 0 - Countries 1:50M version 4.1.0
https://www.naturalearthdata.com

Example of usage:
https://commons.wikimedia.org/wiki/File:Pinus_sylvestris_range.svg

Copyright:
Creative Commons Attribution 4.0 International (CC-BY 4.0)
https://creativecommons.org/licenses/by/4.0/deed.en

Please cite as:
Caudullo, G., Welk, E., San-Miguel-Ayanz, J., 2017. Chorological maps for the main European woody species. Data in Brief 12, 662-666. DOI: https://doi.org/10.1016/j.dib.2017.05.007

last update: 11 Jun 2021